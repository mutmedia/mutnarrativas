﻿using UnityAtoms;
using UnityEngine;

namespace MutCommon
{
  public class FollowGameObjectWithName : MonoBehaviour
  {
    [SerializeField]
    private StringVariable target;

    private Transform transformToFollow;

    void Start()
    {
      if (target == null)
        target = ScriptableObject.CreateInstance<StringVariable>();
      if (target.Changed == null)
      {
        target.Changed = ScriptableObject.CreateInstance<StringEvent>();
      }

      target.Changed.Register(SetFollowToNamedObject);

      SetFollowToNamedObject(target.Value);

      void SetFollowToNamedObject(string n) => transformToFollow = GameObject.Find(n)?.transform;
    }

    void Update()
    {
      if (transformToFollow == null) return;
      this.transform.position = transformToFollow.position;
    }
  }
}