using UnityAtoms;
using UnityEngine;
/// An extremely simple implementation of DialogueUnityVariableStorage, which
/// just stores everything in a Dictionary.
namespace MutNarrativas
{
  /// A default value to apply when the object wakes up, or
  /// when ResetToDefaults is called
  [System.Serializable]
  public class DialogueVariable
  {
    public string Name;

    [SerializeField]
    public StringVariable Variable;

    public string Value
    {
      get => Variable.Value;
      set
      {
        Variable.SetValue(value);
      }
    }

    public Yarn.Value.Type Type;
  }
}