using System;
using System.Linq;
using System.Collections;
using MutPersonagens;
using UnityAtoms;
using UnityEngine;

namespace MutNarrativas.Commands
{
  internal static class EventCommands
  {
    static UnityEngine.ScriptableObject[] _cachedEvents;

    static UnityEngine.ScriptableObject[] cachedEvents
    {
      get
      {
        if (_cachedEvents == null) _cachedEvents = Resources.LoadAll<ScriptableObject>("Events");
        return _cachedEvents;
      }
    }

    internal static IEnumerator TriggerEvent(string[] parameters)
    {
      if (parameters.Length != 1 && parameters.Length != 2)
      {
        UberDebug.LogErrorChannel("mutnarrativas", $"The Event command is with incorrect parameters {parameters.Length}");
        yield break;
      }

      var eventName = parameters[0];

      var eventTriggered = cachedEvents.FirstOrDefault(e => e.name.ToLower() == eventName.ToLower());

      if (eventTriggered == null)
      {
        _cachedEvents = null;
        eventTriggered = cachedEvents.FirstOrDefault(e => e.name.ToLower() == eventName.ToLower());
        if (eventTriggered == null)
        {
          UberDebug.LogErrorChannel("mutnarrativas", $"The Event named {eventName} does not exist.");
          yield break;
        }
      }

      bool found = false;
      if (parameters.Length == 1)
      {
        if (eventTriggered is VoidEvent)
        {
          var voidEvent = eventTriggered as VoidEvent;
          voidEvent.Raise();
          found = true;
        }
      }
      else
      {
        var param = parameters[1];

        if (eventTriggered is StringEvent)
        {
          var stringEvent = eventTriggered as StringEvent;
          stringEvent.Raise(param);
          found = true;
        }
        else if (eventTriggered is IntEvent)
        {
          var ev = eventTriggered as IntEvent;
          if (int.TryParse(param, out int castedParam))
          {
            ev.Raise(castedParam);
            UberDebug.LogChannel("mutnarrativas", $"Could run event {eventName} with parameter \"{castedParam}\" given");
            found = true;
          }
          else
          {
            UberDebug.LogErrorChannel("mutnarrativas", $"Could not run event {eventName} of type int with parameter \"{param}\" given");
            yield break;
          }
        }
        else if (eventTriggered is FloatEvent)
        {
          var ev = eventTriggered as FloatEvent;
          if (float.TryParse(param, out float castedParam))
          {
            ev.Raise(castedParam);
            found = true;
          }
          else
          {
            UberDebug.LogErrorChannel("mutnarrativas", $"Could not run event {eventName} of type float with parameter \"{param}\" given");
            yield break;
          }
        }
        else if (eventTriggered is BoolEvent)
        {
          var ev = eventTriggered as BoolEvent;
          if (bool.TryParse(param, out bool castedParam))
          {
            ev.Raise(castedParam);
            found = true;
          }
          else
          {
            UberDebug.LogErrorChannel("mutnarrativas", $"Could not run event {eventName} of type bool with parameter \"{param}\" given");
            yield break;
          }
        }
        else if (eventTriggered is GameObjectEvent)
        {
          var ev = eventTriggered as GameObjectEvent;
          var castedParam = GameObject.Find(param);
          if (castedParam != null)
          {
            ev.Raise(castedParam);
            found = true;
          }
          else
          {
            UberDebug.LogErrorChannel("mutnarrativas", $"Could not run event {eventName} of type gameobject with parameter \"{param}\" given (could not find that game object)");
            yield break;
          }
        }
      }

      if (!found)
      {
        UberDebug.LogErrorChannel("mutnarrativas", $"The Event named {eventName} with the current implemented type (Void, String, Int, Bool) could not be found with given parameters.");
        yield break;
      }
      yield return null;
    }
  }
}