﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using UnityAtoms;
using System;
using MutNarrativas.Commands;
//using MutCommon.ScriptableObjects;

namespace MutNarrativas
{
  /// Displays dialogue lines to the player, and sends
  /// user choices back to the dialogue system.

  /** Note that this is just one way of presenting the
   * dialogue to the user. The only hard requirement
   * is that you provide the RunLine, RunOptions, RunCommand
   * and DialogueComplete coroutines; what they do is up to you.
   */
  public class DialogueSystem : Yarn.Unity.DialogueUIBehaviour
  {

    /// A delegate (ie a function-stored-in-a-variable) that
    /// we call to tell the dialogue system about what option
    /// the user selected
    private Yarn.OptionChooser SetSelectedOption;

    [Header("Text Interaction")]
    /// How quickly to show the text, in seconds per character
    [Tooltip("How quickly to show the text, in seconds per character")]
    public float textSpeed = 0.025f;
    public float MinTextTime = 1.0f;

    public StringList DialogueOptions;

    public List<KeyCode> interactionKeys = new List<KeyCode>() {
        KeyCode.Space
    };
    public bool AnyKeyUp => interactionKeys.Select(k => Input.GetKeyUp(k)).Aggregate((a, c) => a || c);
    public bool AnyKeyDown => interactionKeys.Select(k => Input.GetKeyDown(k)).Aggregate((a, c) => a || c);


    [Header("Narrator")]
    public string NarratorName = "Narrator";
    public bool UseNarratorAsNamelessCharacter = true;
    public bool UseNarratorForUndefinedCharacter = true;
    public StringVariable NarratorText;
    public StringEvent NarratorTextEvent;

    [Header("Variables")]

    public StringVariable CurrentSpeaker;
    public StringList CurrentParticipants;

    [Header("Events")]
    public VoidEvent OnDialogueStarted;
    public VoidEvent OnDialogueComplete;

    void Awake()
    {
    }

    /// Show a line of dialogue, gradually
    public override IEnumerator RunLine(Yarn.Line line)
    {
      string speakerName, lineString;
      var split = line.text.Split(new[] { ':' }, 2);

      #region Find Current Speaker
      // checar aqui o dialogo do "narrador" => caixa de dialogo generica
      if (split.Count() == 2)
      {
        speakerName = split[0];
        lineString = split[1];
      }
      else
      {
        speakerName = "";
        lineString = line.text;
      }

      StringVariable textVar;
      // TODO: procurar entre os uniques numa lista de uniques da cena O.O
      if (speakerName == "" && UseNarratorAsNamelessCharacter)
      {
        speakerName = NarratorName;
        textVar = NarratorText;
      }
      else if (speakerName == NarratorName)
      {
        textVar = NarratorText;
      }
      else if (Interactable.TryFind(speakerName, out Interactable speaker))
      {
        CurrentParticipants.Add(speaker.Name);
        textVar = speaker.Text;
      }
      else if (speakerName != NarratorName && !UseNarratorForUndefinedCharacter)
      {
        UberDebug.LogErrorChannel("mutnarrativas", $"No character named {speakerName}");
        yield break;
      }
      else
      {
        UberDebug.LogWarningChannel("mutnarrativas", $"No character named {speakerName}, sending to narrator");
        textVar = NarratorText;
        lineString = line.text;
        speakerName = NarratorName;
      }

      CurrentSpeaker.Value = speakerName;

      #endregion

      bool skippedText = false;

      var timer = new Stopwatch();
      timer.Start();

      if (textSpeed > 0.0f)
      {
        // Display the line one character at a time
        var stringBuilder = new StringBuilder();

        foreach (char c in lineString)
        {
          stringBuilder.Append(c);
          textVar.Value = stringBuilder.ToString();
          for (float t = 0; t < textSpeed; t += Time.deltaTime)
          {
            yield return null;
            if (AnyKeyDown)
            {
              textVar.Value = lineString;
              skippedText = true;
              break;
            }
          }
          if (skippedText) break;
        }
      }
      else
      {
        // Display the line immediately if textSpeed == 0
        textVar.Value = line.text;
      }

      yield return new WaitForSeconds(Mathf.Max(0, (float)(MinTextTime - timer.Elapsed.TotalSeconds)));

      // Show the 'press any key' prompt when done, if we have one
      /* 
      if (continuePrompt != null)
        continuePrompt.SetActive(true);
      */

      if (skippedText)
      {
        while (!AnyKeyUp)
        {
          yield return null;
        }
      }

      // Wait for any user input
      while (!AnyKeyDown)
      {
        yield return null;
      }

      /* 
      if (continuePrompt != null)
        continuePrompt.SetActive(false);
      */

      textVar.Value = "";
    }

    /// Show a list of options, and wait for the player to make a selection.
    public override IEnumerator RunOptions(Yarn.Options optionsCollection,
                                            Yarn.OptionChooser optionChooser)
    {
      DialogueOptions.Clear();

      // Display each option in a button, and make it visible
      int i = 0;
      foreach (var optionString in optionsCollection.options)
      {
        if (DialogueOptions != null)
        {
          DialogueOptions.Add(optionString);
        }

        i++;
      }

      // Record that we're using it
      SetSelectedOption = optionChooser;

      // Wait until the chooser has been used and then removed (see SetOption below)
      while (SetSelectedOption != null)
      {
        yield return null;
      }

      DialogueOptions.Clear();
    }

    /// Called by buttons to make a selection.
    public void SetOption(int selectedOption)
    {
      if (selectedOption < 0) return;
      if (SetSelectedOption == null) return;

      // Call the delegate to tell the dialogue system that we've
      // selected an option.
      SetSelectedOption(selectedOption);

      // Now remove the delegate so that the loop in RunOptions will exit
      SetSelectedOption = null;
    }

    /// Run an internal command.
    public override IEnumerator RunCommand(Yarn.Command command)
    {
      var parsed = command.text.Split();

      bool isAsync = parsed[0] == "async";
      int commandIndex = isAsync ? 1 : 0;

      var commandName = parsed[commandIndex];
      var parameters = parsed.Skip(commandIndex + 1).ToArray();

      var co = StartCoroutine(DialogueSystemCommands.ExecuteCommand(commandName, parameters));

      if (!isAsync)
      {
        yield return co;
      }
    }

    /// Called when the dialogue system has started running.
    public override IEnumerator DialogueStarted()
    {
      yield return null;
      OnDialogueStarted.Raise();
      UberDebug.LogChannel("mutnarrativas", "Dialogue Starting!");
      // Hide the game controls.
      yield break;
    }

    /// Called when the dialogue system has finished running.
    public override IEnumerator DialogueComplete()
    {
      UberDebug.LogChannel("mutnarrativas", "Dialogue Complete!");
      OnDialogueComplete.Raise();
      CurrentParticipants.Clear();
      CurrentSpeaker.Value = "";
      yield break;
    }
  }

}
