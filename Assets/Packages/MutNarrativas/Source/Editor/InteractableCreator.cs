﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityAtoms;
using System;
using Object = UnityEngine.Object;
using System.IO;
using MutCommon;
using System.Linq;

namespace MutNarrativas.Editor
{
  public enum InteractableCreationType
  {
    Object,
    Character,
    Item,
    //TODO: arrastar uma cena
    // TODO: poder chegar num ponto especifico de uma cena
    Exit,
  }

  public class InteractableCreator : EditorWindow
  {

    const int UI_PRIORITY = 0;
    InteractableModel model;

    private Func<string, string> yarnTemplate;

    private string assetPath;


    private Action<Interactable> additionalInteractableSetup;

    protected GUIContent nameContent = new GUIContent("Name");
    protected GUIContent visualRepresentationContent = new GUIContent("Visual Representation");
    protected GUIContent DialogueBox = new GUIContent("Dialogue Box");
    protected GUIContent createContent = new GUIContent("Create");
    protected InteractableCreationType interactableType;

    protected GameObject emptyInteractable;

    [MenuItem("Window/MutNarrativas/Character")]
    [MenuItem("MutNarrativas/New Character...", false, UI_PRIORITY)]
    static void NewCharacter()
    {
      InteractableCreator window = GetWindow<InteractableCreator>();

      window.titleContent.text = "Create Character (Mut Narrativas)";
      window.yarnTemplate = MutNarrativasSettings.S.CharacterYarnTemplate;
      window.assetPath = MutNarrativasSettings.S.CharacterAssetsPath;

      window.model.IsUnique = true;
      window.interactableType = InteractableCreationType.Character;
      window.emptyInteractable = MutNarrativasSettings.S.EmptyCharacterGameObject;

      window.Show();
    }

    [MenuItem("Window/MutNarrativas/Item")]
    [MenuItem("MutNarrativas/New Item...", false, UI_PRIORITY)]
    static void NewItem()
    {
      InteractableCreator window = GetWindow<InteractableCreator>();

      window.titleContent.text = "Create Item (Mut Narrativas)";
      window.yarnTemplate = MutNarrativasSettings.S.ItemYarnTemplate;
      window.assetPath = MutNarrativasSettings.S.ItemAssetPath;

      window.model.IsUnique = false;
      window.interactableType = InteractableCreationType.Item;
      window.emptyInteractable = MutNarrativasSettings.S.EmptyItemGameObject;
      window.additionalInteractableSetup = (interactable) =>
      {
        //TODO: create item variable and set
        var item = interactable.GetComponent<Item>();
        //StringVariable IntReferece;
        if (item == null) return;
      };

      window.Show();
    }

    [MenuItem("Window/MutNarrativas/Interactable Object")]
    [MenuItem("MutNarrativas/New Interactable Object...", false, UI_PRIORITY)]
    static void NewInteractableObject()
    {
      InteractableCreator window = GetWindow<InteractableCreator>();

      window.titleContent.text = "Create Object (Mut Narrativas)";
      window.yarnTemplate = MutNarrativasSettings.S.InteractableYarnTemplate;
      window.assetPath = MutNarrativasSettings.S.ObjectAssetPath;

      window.model.IsUnique = false;
      window.interactableType = InteractableCreationType.Object;
      window.emptyInteractable = MutNarrativasSettings.S.EmptyInteractableGameObject;

      window.Show();
    }

    // TODO: saidas sao mais complicadas. Podem existir varias saidas, a boa 'e que exista um jeito de criar entradas na cena alvo
    //[MenuItem("Window/MutNarrativas/Exit")]
    //[MenuItem("MutNarrativas/New Exit...")]
    static void NewExit()
    {
      InteractableCreator window = GetWindow<InteractableCreator>();

      window.titleContent.text = "Create Object (Mut Narrativas)";
      window.yarnTemplate = MutNarrativasSettings.S.ExitYarnTemplate;
      window.assetPath = MutNarrativasSettings.S.ExitAssetPath;

      window.model.IsUnique = false;
      window.interactableType = InteractableCreationType.Exit;
      window.emptyInteractable = MutNarrativasSettings.S.EmptyInteractableGameObject;

      //window.nameContent = new GUIContent("Target Scene");

      window.Show();
    }

    private void Awake()
    {
      model = new InteractableModel
      {
        DialogueBox = MutNarrativasSettings.S.DefaultCharacterDialogueBox.GetComponent<CharacterDialogueBox>(),
        CanInteract = true,
        UseCustomNode = false,
        CustomTalkToNode = "",
        Name = "",
        IsUnique = true,
      };
    }

    void OnGUI()
    {
      model.Name = EditorGUILayout.TextField(nameContent, model.Name);
      model.Name = model.Name.MinimizeWhiteSpace().Replace(" ", "_");
      model.VisualRepresentation = (GameObject)EditorGUILayout.ObjectField(visualRepresentationContent, model.VisualRepresentation, typeof(GameObject), false);
      model.DialogueBox = (CharacterDialogueBox)EditorGUILayout.ObjectField(DialogueBox, model.DialogueBox, typeof(CharacterDialogueBox), false);

      //additionalGUI?.Invoke();

      if (string.IsNullOrWhiteSpace(model.Name))
      {
        return;
      }

      if (model.VisualRepresentation == null)
      {
        return;
      }

      if (GUILayout.Button(createContent))
        Create(model, assetPath);
    }


    private void Create(InteractableModel model, string characterAssetPath)
    {
      AssetDatabaseHelper.CreateFolderIfNotExists(characterAssetPath);
      characterAssetPath += $"/{model.Name}";

      if (!AssetDatabaseHelper.CreateFolderIfNotExists(characterAssetPath))
      {
        var ok = EditorUtility.DisplayDialog("Warning",
                    $"{interactableType} named {model.Name} already exists, using existing {interactableType}",
                    "OK", "Cancel");
        if (!ok) return;
        Instantiate(AssetDatabase.LoadAssetAtPath<Interactable>(characterAssetPath));
        return;
      }

      bool success;

      // Create event and text
      {
        var characterTextEvent = ScriptableObject.CreateInstance<StringEvent>();
        (characterTextEvent, success) = SaveOrLoadExistingAsset<StringEvent>(characterTextEvent, "textEvent_");
        if (!success) return;

        var characterText = ScriptableObject.CreateInstance<StringVariable>();
        characterText.Changed = characterTextEvent;
        (characterText, success) = SaveOrLoadExistingAsset<StringVariable>(characterText, "text_");
        if (!success) return;

        model.Text = characterText;
      }

      // Create Dialogue Character Game Object
      var interactablesContainer = GameObject.FindGameObjectWithTag("InteractablesContainer");
      if (interactablesContainer == null)
      {
        UberDebug.LogErrorChannel("mutnarrativas", "No CharactersContainer object found");
        return;
      }

      GameObject interactableGO = Instantiate(emptyInteractable, interactablesContainer.transform);
      interactableGO.name = model.Name;

      Interactable interactable = interactableGO.GetComponent<Interactable>() ?? interactableGO.AddComponent<Interactable>();
      interactable.OnAnyDialogueStarted = MutNarrativasSettings.S.OnDialogueStarted;
      interactable.OnAnyDialogueComplete = MutNarrativasSettings.S.OnDialogueComplete;

      var visualRepresentation = model.VisualRepresentation == null ? new GameObject() : Instantiate(model.VisualRepresentation);
      visualRepresentation.transform.parent = interactableGO.transform;
      visualRepresentation.transform.localPosition = Vector3.zero;
      visualRepresentation.transform.localRotation = Quaternion.identity;
      visualRepresentation.transform.localScale = Vector3.one;
      visualRepresentation.name = $"{model.Name}Visual";
      model.VisualRepresentation = visualRepresentation;

      // Save text
      if (model.CanInteract && model.ScriptToLoad == null)
      {

        TextAsset textAsset = new TextAsset();

        var yarnString = yarnTemplate(model.Name);
        (textAsset, success) = textAsset.SaveOrLoadExisting<TextAsset>($"{model.Name}.yarn", characterAssetPath, ".txt", (_, p) =>
        {
          File.WriteAllText(p, yarnString);
        });
        if (!success) return;

        //= (TextAsset)Resources.Load($"{MutDialogosSettings.S.CharacterYarnScriptsResourceLocation}/{model.CharacterName}.yarn", typeof(TextAsset));
        model.ScriptToLoad = textAsset;
        model.UseCustomNode = true;

        using (var sr = new StringReader(yarnString))
        {
          var firstLine = sr.ReadLine();
          var name = firstLine.Split(':')[1].Trim();
          model.CustomTalkToNode = name;
        }
      }

      interactable.Setup(model);
      additionalInteractableSetup?.Invoke(interactable);

      // Save prefab
      interactableGO.SaveOrLoadExisting<GameObject>($"prefab_{model.Name}", characterAssetPath, ".prefab", (o, p) =>
      {
        PrefabUtility.SaveAsPrefabAssetAndConnect(o, p, InteractionMode.AutomatedAction);
      });

      // Finally
      AssetDatabase.SaveAssets();
      AssetDatabase.Refresh();
      Close();

      return;

      (T, bool) SaveOrLoadExistingAsset<T>(T asset, string resourcePrefix) where T : Object
      {
        return asset.SaveOrLoadExisting<T>($"{resourcePrefix}{model.Name}", characterAssetPath, ".asset", (a, p) => AssetDatabase.CreateAsset(a, p));
      }

    }
  }
}