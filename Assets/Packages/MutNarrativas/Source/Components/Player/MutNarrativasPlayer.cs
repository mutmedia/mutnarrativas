using MutPersonagens;
using UnityEngine;

namespace MutNarrativas.Player
{
  [RequireComponent(typeof(PlayerController))]
  [RequireComponent(typeof(PlayerInteraction))]
  [RequireComponent(typeof(LockPlayerOnDialogue))]
  public class MutNarrativasPlayer : MonoBehaviour
  {
  }
}