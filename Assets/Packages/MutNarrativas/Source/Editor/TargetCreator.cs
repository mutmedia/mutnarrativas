using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;
using MutCommon;
using System.IO;
using Yarn.Unity;

namespace MutNarrativas
{
  public class TargetCreator : EditorWindow
  {
    protected string targetName = "";

    protected readonly GUIContent m_NameContent = new GUIContent("Target Name");

    [MenuItem("Window/MutNarrativas/Target")]
    [MenuItem("MutNarrativas/New Targets...", false, 20)]
    static void Init()
    {
      TargetCreator window = GetWindow<TargetCreator>();
      window.titleContent.text = "New Target (Mut Narrativas)";
      window.Show();
    }

    static void Create(string name)
    {
      var t = Instantiate(MutNarrativasSettings.S.TargetPrefab);
      t.name = name;
    }

    void OnGUI()
    {
      targetName = targetName.ToLower().MinimizeWhiteSpace().Replace(" ", "_");
      targetName = EditorGUILayout.TextField(m_NameContent, targetName);

      if (string.IsNullOrWhiteSpace(targetName))
      {
        return;
      }

      if (GUILayout.Button("Create"))
      {
        Create(targetName);
        targetName = "";
      }
    }
  }
}