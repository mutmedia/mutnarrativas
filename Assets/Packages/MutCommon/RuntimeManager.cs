﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MutCommon
{
  public class RuntimeManager : MonoBehaviour
  {
    public void CloseGame()
    {
      Application.Quit();
    }
  }
}