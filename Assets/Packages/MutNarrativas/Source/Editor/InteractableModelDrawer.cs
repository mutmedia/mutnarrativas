using UnityEditor;
using UnityEngine;

namespace MutNarrativas
{
  [CustomPropertyDrawer(typeof(InteractableModel))]
  public class DialogueCharacterModelDrawer : PropertyDrawer
  {
    private bool More = false;
    public override float GetPropertyHeight(SerializedProperty prop,
                                                 GUIContent label)
    {
      float totalHeight = 0;

      // Character Text
      totalHeight += EditorGUIUtility.singleLineHeight;
      // Interactable
      totalHeight += EditorGUIUtility.singleLineHeight;

      if (prop.FindPropertyRelative("CanInteract").boolValue)
      {
        //ScriptToLoad
        totalHeight += EditorGUIUtility.singleLineHeight;
        //UseCustomNode
        totalHeight += EditorGUIUtility.singleLineHeight;

        if (prop.FindPropertyRelative("UseCustomNode").boolValue)
        {
          totalHeight += EditorGUIUtility.singleLineHeight;
        }
      }

      // More...
      totalHeight += EditorGUIUtility.singleLineHeight;
      if (More)
      {
        totalHeight += EditorGUIUtility.singleLineHeight;
        totalHeight += EditorGUIUtility.singleLineHeight;
        totalHeight += EditorGUIUtility.singleLineHeight;
        totalHeight += EditorGUIUtility.singleLineHeight;
      }

      return totalHeight;
    }

    public override void OnGUI(Rect pos, SerializedProperty prop, GUIContent label)
    {
      EditorGUI.BeginProperty(pos, label, prop);

      pos.height = EditorGUIUtility.singleLineHeight;
      //position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

      // Don't make child fields be indented
      var indent = EditorGUI.indentLevel;
      EditorGUI.indentLevel = 0;

      EditorGUI.PropertyField(pos, prop.FindPropertyRelative("Name"));
      pos.y += EditorGUIUtility.singleLineHeight;

      var interactable = prop.FindPropertyRelative("CanInteract");
      EditorGUI.PropertyField(pos, interactable);
      pos.y += EditorGUIUtility.singleLineHeight;

      if (interactable.boolValue)
      {
        EditorGUI.PropertyField(pos, prop.FindPropertyRelative("ScriptToLoad"));
        pos.y += EditorGUIUtility.singleLineHeight;


        //EditorGUILayout.PropertyField(property.FindPropertyRelative("ScriptsToLoad"));
        var useCustomNode = prop.FindPropertyRelative("UseCustomNode");

        EditorGUI.PropertyField(pos, useCustomNode);
        pos.y += EditorGUIUtility.singleLineHeight;

        if (useCustomNode.boolValue)
        {
          EditorGUI.PropertyField(pos, prop.FindPropertyRelative("CustomTalkToNode"));
          pos.y += EditorGUIUtility.singleLineHeight;
        }
      }

      More = EditorGUI.Toggle(pos, "More? (Debug)", More);
      pos.y += EditorGUIUtility.singleLineHeight;

      if (More)
      {
        EditorGUI.PropertyField(pos, prop.FindPropertyRelative("Text"));
        pos.y += EditorGUIUtility.singleLineHeight;
        EditorGUI.PropertyField(pos, prop.FindPropertyRelative("VisualRepresentation"));
        pos.y += EditorGUIUtility.singleLineHeight;
        EditorGUI.PropertyField(pos, prop.FindPropertyRelative("DialogueBox"));
        pos.y += EditorGUIUtility.singleLineHeight;
        EditorGUI.PropertyField(pos, prop.FindPropertyRelative("IsUnique"));
        pos.y += EditorGUIUtility.singleLineHeight;
      }

      // Set indent back to what it was
      EditorGUI.indentLevel = indent;

      EditorGUI.EndProperty();
    }
  }
}