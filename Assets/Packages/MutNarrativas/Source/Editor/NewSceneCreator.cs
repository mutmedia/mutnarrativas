using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;
using MutCommon;
using System.IO;
using Yarn.Unity;

namespace MutNarrativas
{
  public class NewSceneCreator : EditorWindow
  {
    protected string sceneName;

    protected readonly GUIContent m_NameContent = new GUIContent("New Scene Name");

    [MenuItem("Window/MutNarrativas/Scene")]
    [MenuItem("MutNarrativas/New Scene...", false, 20)]
    static void Init()
    {
      NewSceneCreator window = GetWindow<NewSceneCreator>();
      window.titleContent.text = "New Scene (Mut Narrativas)";
      window.Show();
    }

    void OnGUI()
    {
      sceneName = EditorGUILayout.TextField(m_NameContent, sceneName);

      if (GUILayout.Button("Create"))
        CheckAndCreateScene();
    }

    protected void CheckAndCreateScene()
    {
      if (EditorApplication.isPlaying)
      {
        UberDebug.LogWarningChannel("mutnarrativas", "Cannot create scenes while in play mode.  Exit play mode first.");
        return;
      }

      Scene currentActiveScene = UnityEngine.SceneManagement.SceneManager.GetActiveScene();

      if (currentActiveScene.isDirty)
      {
        string title = currentActiveScene.name + " Has Been Modified";
        string message = "Do you want to save the changes you made to " + currentActiveScene.path + "?\nChanges will be lost if you don't save them.";
        int option = EditorUtility.DisplayDialogComplex(title, message, "Save", "Don't Save", "Cancel");

        if (option == 0)
        {
          EditorSceneManager.SaveScene(currentActiveScene);
        }
        else if (option == 2)
        {
          return;
        }
      }

      CreateScene();
    }

    protected void CreateScene()
    {
      string[] result = AssetDatabase.FindAssets("MutNarrativasTemplateScene");

      if (result.Length > 0)
      {

        #region Create Scene
        string newSceneDirectory = $"{MutNarrativasSettings.S.ScenesPath}/{sceneName}";
        string newScenePath = $"{newSceneDirectory}/{sceneName}.unity";
        AssetDatabaseHelper.CreateFolderIfNotExists(newSceneDirectory);
        AssetDatabase.CopyAsset(AssetDatabase.GUIDToAssetPath(result[0]), newScenePath);
        AssetDatabase.Refresh();
        Scene newScene = EditorSceneManager.OpenScene(newScenePath, OpenSceneMode.Single);
        #endregion

        #region Create Yarn File for Scene
        // Create scene text
        TextAsset textAsset = new TextAsset();
        bool success;

        (textAsset, success) = textAsset.SaveOrLoadExisting<TextAsset>($"{sceneName}_Scene.yarn", newSceneDirectory, ".txt", (_, p) =>
        {
          File.WriteAllText(p, MutNarrativasSettings.S.SceneYarnTemplate(sceneName));
        });

        if (!success)
        {
          UberDebug.LogErrorChannel("mutnarrativas", "Could not create scene yarn asset");
          return;
        }

        var dialogueRunner = FindObjectOfType<DialogueRunner>();
        if (dialogueRunner == null)
        {
          UberDebug.LogErrorChannel("mutnarrativas", "Could not find a dialogue runner in the scene");
          return;
        }
        dialogueRunner.sourceText = new TextAsset[] { textAsset };
        dialogueRunner.startAutomatically = true;
        dialogueRunner.startNode = $"scene.{sceneName}.Main";
        EditorUtility.SetDirty(dialogueRunner);

        #endregion

        #region Add to scene settings
        {
          EditorBuildSettingsScene[] buildScenes = EditorBuildSettings.scenes;

          EditorBuildSettingsScene[] newBuildScenes = new EditorBuildSettingsScene[buildScenes.Length + 1];
          for (int i = 0; i < buildScenes.Length; i++)
          {
            newBuildScenes[i] = buildScenes[i];
          }
          newBuildScenes[buildScenes.Length] = new EditorBuildSettingsScene(newScene.path, true);
          EditorBuildSettings.scenes = newBuildScenes;
        }
        #endregion

        Close();
      }
      else
      {
        EditorUtility.DisplayDialog("Error",
            "The scene MutNarrativasTemplateScene was not found. This scene is required by the New Scene Creator.",
            "OK");
      }
    }

    protected void AddSceneToBuildSettings(Scene scene)
    {

    }
  }
}