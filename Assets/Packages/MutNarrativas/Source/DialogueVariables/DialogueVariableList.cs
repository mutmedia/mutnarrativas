using UnityAtoms;
using UnityEngine;
/// An extremely simple implementation of DialogueUnityVariableStorage, which
/// just stores everything in a Dictionary.
namespace MutNarrativas
{
  [CreateAssetMenu(menuName = "MutNarrativas/DialogueVariable/List", fileName = "DialogueVariableList", order = 1)]
  public class DialogueVariableList : UnityAtoms.AtomList<DialogueVariable, DialogueVariableEvent>
  {
  }

}