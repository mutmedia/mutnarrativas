using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Yarn.Unity;
using System.Linq;
using UnityAtoms;
using MutPersonagens;

namespace MutNarrativas.Player
{
  [RequireComponent(typeof(PlayerController))]
  public class LockPlayerOnDialogue : MonoBehaviour
  {
    [SerializeField]
    private VoidEvent OnDialogueStart;

    [SerializeField]
    private VoidEvent OnDialogueComplete;

#if UNITY_EDITOR
    private void OnValidate()
    {
      if (OnDialogueStart == null)
      {
        OnDialogueStart = MutNarrativasSettings.S.OnDialogueStarted;
      }
      if (OnDialogueComplete == null)
      {
        OnDialogueComplete = MutNarrativasSettings.S.OnDialogueComplete;
      }
    }
#endif

    private void Awake()
    {
      var player = GetComponent<PlayerController>();
      OnDialogueStart.RegisterAction((_) => player.Lock());
      OnDialogueComplete.RegisterAction((_) => player.Unlock());
    }
  }
}