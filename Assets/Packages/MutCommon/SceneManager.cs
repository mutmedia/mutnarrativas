﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MutCommon
{
  public class SceneManager : MonoBehaviour
  {
    public void SetScene(string sceneName)
    {
      if (Application.CanStreamedLevelBeLoaded(sceneName))
      {
        UnityEngine.SceneManagement.SceneManager.LoadScene(sceneName);
      }
      else
      {
        UberDebug.LogErrorChannel("mutcommon", "No scene with name: " + sceneName);
      }
    }
  }
}