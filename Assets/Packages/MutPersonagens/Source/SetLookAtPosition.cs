﻿using UnityAtoms;
using UnityEngine;

namespace MutPersonagens
{
  [ExecuteInEditMode]
  [RequireComponent(typeof(Animator))]
  public class SetLookAtPosition : MonoBehaviour
  {
    [SerializeField]
    private BoolReference isLooking;

    [SerializeField]
    private Transform target;

    private Animator animator;

    void Awake()
    {
      animator = GetComponent<Animator>();
    }

    private void OnAnimatorIK(int layerIndex)
    {
      if (target == null) return;

      if (isLooking.Value)
      {
        animator.SetLookAtWeight(1);
        animator.SetLookAtPosition(target.position);
      }
      else
      {
        animator.SetLookAtWeight(0);
      }
    }
  }
}