
using System;
using UnityAtoms;
using UnityEditor;
using UnityEngine;
using UnityEngine.Serialization;

namespace MutNarrativas
{
    [CreateAssetMenu(fileName = "MutNarrativasSettingsDefault", menuName = "MutNarrativas/Settings", order = 1)]
    public class MutNarrativasSettings : ScriptableObject
    {
#if UNITY_EDITOR

        [MenuItem("MutNarrativas/Settings...", false, 100)]
        static void Init()
        {
            Selection.activeObject = MutNarrativasSettings.S;
        }
        static Lazy<MutNarrativasSettings> LazySettings = new Lazy<MutNarrativasSettings>(() =>
            {
                var result = AssetDatabase.FindAssets("MutNarrativasSettingsDefault");
                var path = AssetDatabase.GUIDToAssetPath(result[0]);
                return AssetDatabase.LoadAssetAtPath<MutNarrativasSettings>(path);
            });

        public static MutNarrativasSettings S => LazySettings.Value;
#endif

        [Header("Game Objects/Prefabs")]
        public GameObject DefaultCharacterDialogueBox;
        //public CharacterDialogueBox DefaultCharacterDialogueBoxComp;

        public GameObject EmptyInteractableGameObject;
        public GameObject EmptyItemGameObject;
        public GameObject EmptyCharacterGameObject;

        public GameObject TargetPrefab;
        [Header("Narrator")]

        public string NarratorName = "Narrator";
        public bool UseNarratorAsNamelessCharacter = true;
        public bool UseNarratorForUndefinedCharacter = true;
        public StringVariable NarratorText;
        public StringEvent NarratorTextEvent;

        [Header("Paths")]
        public string CharacterAssetsPath = "Assets/Resources/Characters";
        public string ItemAssetPath = "Assets/Resources/Items";
        public string ObjectAssetPath = "Assets/Resources/Objects";
        public string ExitAssetPath = "Assets/Resources/Exits";
        public string ScenesPath = "Assets/Resources/Scenes";
        internal string LocalizationPath = "Assets/Resources/Localization";

        [Header("Yarn Templates")]
        public string YarnTemplateNameMarkup = "{name}";

#pragma warning disable CS0649
        [SerializeField]
        private TextAsset characterYarnTemplate;
        public Func<string, string> CharacterYarnTemplate => FillTemplateWithNameFunc(characterYarnTemplate);
        [SerializeField]
        private TextAsset itemYarnTemplate;
        public Func<string, string> ItemYarnTemplate => FillTemplateWithNameFunc(itemYarnTemplate);
        [SerializeField]
        private TextAsset interactableYarnTemplate;

        public Func<string, string> InteractableYarnTemplate => FillTemplateWithNameFunc(interactableYarnTemplate);

        [SerializeField]
        private TextAsset exitYarnTemplate;
        public Func<string, string> ExitYarnTemplate => FillTemplateWithNameFunc(exitYarnTemplate);

        [SerializeField]
        private TextAsset sceneYarnTemplate;
        public Func<string, string> SceneYarnTemplate => FillTemplateWithNameFunc(sceneYarnTemplate);
#pragma warning restore CS0649

        public string CharacterTextLocation(string name) => $"{CharacterAssetsPath}/{name}";
        Func<string, string> FillTemplateWithNameFunc(TextAsset t) => (name) => t.text.Replace(YarnTemplateNameMarkup, name);
        //public GameObject DefaultDialogueCharacter;

        [Header("Events")]
        public VoidEvent OnDialogueComplete;
        public VoidEvent OnDialogueStarted;


    }
}