﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class TransformFollower : MonoBehaviour
{

  [SerializeField]
  public Transform ThingToFollow;

  [SerializeField]
  public Vector3 Offset = new Vector3(0, 2.75f, 0);
  // Start is called before the first frame update
  // Update is called once per frame
  void Update()
  {
    if (ThingToFollow == null) return;
    transform.position = ThingToFollow.position + Offset;
  }
}
