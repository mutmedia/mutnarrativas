using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;
using System.Linq;

namespace MutModels
{
  public class KoshaPrefabCreator : EditorWindow
  {
    //[MenuItem("Window/MutModels/Kosha Prefab")]
    //[MenuItem("MutModels/New Kosha Prefab...")]
    static void NewCharacter()
    {
      KoshaPrefabCreator window = GetWindow<KoshaPrefabCreator>();

      window.Show();
    }

    private void Awake()
    {
    }

    [Serializable]
    private struct AnimationNamePair
    {
      public string Name;
      public AnimationClip Animation;
    }

    private static string[] BaseAnimations = new string[] { "idle", "walk" };


    List<AnimationNamePair> anims = BaseAnimations.Select(b => new AnimationNamePair()
    {
      Name = b
    }).ToList();

    string CharacterName;


    void OnGUI()
    {
      CharacterName = EditorGUILayout.TextField("Name", CharacterName);
      EditorGUILayout.Separator();

      GUILayout.Label("Animations");
      for (int i = 0; i < anims.Count; i++)
      {
        var anim = anims[i];
        if (i >= BaseAnimations.Count())
        {
          GUILayout.BeginHorizontal();
          var removeButtonWidth = 20;
          if (GUILayout.Button("X", GUILayout.Width(removeButtonWidth)))
          {
            anims.RemoveAt(i);
            break;
          }
          anim.Name = GUILayout.TextField(anim.Name, GUILayout.ExpandWidth(false), GUILayout.Width(EditorGUIUtility.labelWidth - removeButtonWidth - 4 * EditorGUIUtility.standardVerticalSpacing));
          anim.Animation = (AnimationClip)EditorGUILayout.ObjectField(anim.Animation, typeof(AnimationClip), false);
          GUILayout.EndHorizontal();
        }
        else
        {
          anim.Animation = (AnimationClip)EditorGUILayout.ObjectField(anim.Name, anim.Animation, typeof(AnimationClip), false);
        }
        anims[i] = anim;
      }

      if (GUILayout.Button("Add Custom Animation..."))
      {
        anims.Add(new AnimationNamePair());
      }
    }

    private void Create()
    {
    }
  }
}