﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace MutNarrativas.Commands
{
  [RequireComponent(typeof(AudioSource))]
  public class SoundCommandManager : MonoBehaviour
  {
    public List<AudioClip> songList;

    // TODO: have multiple audio sources?
    private AudioSource sfxSource;

    [SerializeField]
    private Crossfader crossfaderPrefab;

    private Crossfader musicSource;

    private static SoundCommandManager instance = null;

    private bool persist = true;

    [SerializeField]
    private bool GetFromResources;

    public string ResourcesFolder;

    public float fadeOutDuration;

    void Awake()
    {
      // Singleton logic
      {
        if (instance != null && instance != this)
        {
          Destroy(this.gameObject);
          return;
        }
        else
        {
          instance = this;
        }

        if (persist)
        {
          transform.parent = null;
          DontDestroyOnLoad(this.gameObject);
        }
      }

      // Populate variables
      {
        sfxSource = GetComponent<AudioSource>();
        if (sfxSource == null)
        {
          sfxSource = gameObject.AddComponent<AudioSource>();
        }

        musicSource = Instantiate(crossfaderPrefab, this.transform);

        if (GetFromResources)
        {
          var extraClips = Resources.LoadAll<AudioClip>(ResourcesFolder);
          songList.AddRange(extraClips);
        }
      }

      // Register Commands
      DialogueSystemCommands.AddCommand("sound", PlaySound);
      DialogueSystemCommands.AddCommand("sfx", PlaySound);
      DialogueSystemCommands.AddCommand("music", PlayMusic);
    }

    IEnumerator PlayMusic(string[] parameters)
    {
      if (parameters.Count() != 1 && parameters.Count() != 2)
      {
        UberDebug.LogErrorChannel("mutnarrativas", $"The sound command is with incorrect number of parameters, parameters: {parameters}");
        yield break;
      }

      var name = parameters[0].ToLower();
      var song = songList.FirstOrDefault((w) => w.name.ToLower() == name.ToLower());

      if (song == null)
      {
        UberDebug.LogErrorChannel("mutnarrativas", $"There is no song named {name}");
        yield break;
      }

      var crossFadeDuration = 0.0f;
      if (parameters.Count() == 2 && !float.TryParse(parameters[1], out crossFadeDuration))
      {
        UberDebug.LogErrorChannel("mutnarrativas", $"Parameter {parameters[1]} is not a suitable duration");
        yield break;
      }

      musicSource.Crossfade(song, crossFadeDuration);
    }

    IEnumerator PlaySound(string[] parameters)
    {
      if (parameters.Count() != 1)
      {
        UberDebug.LogErrorChannel("mutnarrativas", $"The sound command is with incorrect number of parameters, parameters: {parameters}");
        yield break;
      }

      var name = parameters[0].ToLower();
      var song = songList.FirstOrDefault((w) => w.name.ToLower() == name.ToLower());

      if (song == null)
      {
        UberDebug.LogErrorChannel("mutnarrativas", $"There is no sfx named {name}");
        yield break;
      }

      sfxSource.PlayOneShot(song);
    }
  }
}