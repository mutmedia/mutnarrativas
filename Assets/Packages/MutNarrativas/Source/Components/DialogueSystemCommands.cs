using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using MutPersonagens;
using UnityEngine;

namespace MutNarrativas.Commands
{
  public delegate IEnumerator CommandExecution(string[] parameters);

  public static class DialogueSystemCommands
  {
    #region Implementation

    public static IEnumerator ExecuteCommand(string commandName, string[] parameters)
    {
      if (RegisteredCommands.TryGetValue(commandName, out CommandExecution execution))
      {
        UberDebug.LogChannel("mutnarrativas", "Executing " + commandName);
        yield return execution(parameters);
      }
      else
      {
        yield return NoOp(commandName, parameters);
      }
    }

    private static IEnumerator NoOp(string name, string[] parameters)
    {
      UberDebug.LogWarningChannel("mutnarrativas", $"Function {name} with {parameters.Count()} parameters does not exist");
      yield return null;
    }

    #endregion


    #region Commands

    // TODO: see annotations for better command definition
    private static Dictionary<string, CommandExecution> RegisteredCommands = new Dictionary<string, CommandExecution>()
  {
    // General
    {"wait", Wait},

    // Character
    {"moveto", CharacterCommands.MoveToPosition},
    {"move", CharacterCommands.MoveToPosition},
    {"walk", CharacterCommands.MoveToPositionWithSpeed(50) },
    {"run", CharacterCommands.MoveToPositionWithSpeed(100) },
    {"lookat", CharacterCommands.LookatPosition },
    {"look", CharacterCommands.LookatPosition },
    {"emotion", CharacterCommands.ChangeEmotion },

    // Player
    {"lock", PlayerCommands.PlayerLock},
    {"unlock", PlayerCommands.PlayerUnlock},

    // Events
    // Note that with this everything can be called, no need for new commands

    // TODO:
    //"walktime" with time
    //"camera"
    //"play_video"
  };

    public static void AddCommand(string name, CommandExecution command)
    {
      Debug.Log($"Registering command with name {name}");

      if (RegisteredCommands.ContainsKey(name))
      {
        RegisteredCommands[name] = command;
      }
      else
      {
        RegisteredCommands.Add(name, command);
      }
    }

    private static IEnumerator Wait(string[] parameters)
    {

      if (!float.TryParse(parameters[0], out float waitTime))
      {
        UberDebug.LogErrorChannel("mutnarrativas", "Requested wait time is not a number");
        yield break;
      }
      yield return new WaitForSeconds(waitTime);
    }


    #endregion
  }
}