﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioLowPassFilter))]
[RequireComponent(typeof(AudioHighPassFilter))]
[RequireComponent(typeof(AudioSource))]
public class MusicFader : MonoBehaviour
{
  public AudioSource Source;

  float MAX_FREQ = 22000;
  float MIN_FREQ = 10;

  AudioLowPassFilter lowPass;
  AudioHighPassFilter highPass;

  [SerializeField]
  AnimationCurve HighPassCurveFadeIn;
  [SerializeField]
  AnimationCurve LowPassCurveFadeIn;
  [SerializeField]
  AnimationCurve HighPassCurveFadeOut;
  [SerializeField]
  AnimationCurve LowPassCurveFadeOut;

  public void FadeIn(float time)
  {
    StopAllCoroutines();
    Source.Play();
    StartCoroutine(FadeInCoroutine(time));
  }

  static float Log2(float x) => (Mathf.Log10(x + 1) / Mathf.Log10(2));
  static float Exp2(float x) => Mathf.Pow(2, x) - 1;

  private IEnumerator FadeInCoroutine(float time) => CoroutineHelpers.InterpolateByTime(time,
  (k) =>
  {
    lowPass.cutoffFrequency = Mathf.Lerp(MIN_FREQ, MAX_FREQ, LowPassCurveFadeIn.Evaluate(k));
    highPass.cutoffFrequency = Mathf.Lerp(MAX_FREQ, MIN_FREQ, HighPassCurveFadeIn.Evaluate(k));
  });

  public void FadeOut(float time)
  {
    StopAllCoroutines();
    StartCoroutine(FadeOutCoroutine(time));
  }

  private IEnumerator FadeOutCoroutine(float time) => CoroutineHelpers.InterpolateByTime(time,
  (k) =>
  {
    lowPass.cutoffFrequency = Mathf.Lerp(MAX_FREQ, MIN_FREQ, LowPassCurveFadeOut.Evaluate(k));
    highPass.cutoffFrequency = Mathf.Lerp(MIN_FREQ, MAX_FREQ, HighPassCurveFadeOut.Evaluate(k));
  }, () => Source.Stop());

  // Start is called before the first frame update
  void Awake()
  {
    Source = GetComponent<AudioSource>();
    lowPass = GetComponent<AudioLowPassFilter>();
    highPass = GetComponent<AudioHighPassFilter>();
    MAX_FREQ = lowPass.cutoffFrequency;
    MIN_FREQ = highPass.cutoffFrequency;


    //Start off
    lowPass.cutoffFrequency = MIN_FREQ;
    highPass.cutoffFrequency = MAX_FREQ;
  }
}
