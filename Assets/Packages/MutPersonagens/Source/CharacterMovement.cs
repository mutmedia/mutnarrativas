﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityAtoms;
using UnityEngine.Serialization;

namespace MutPersonagens
{
  [RequireComponent(typeof(CharacterController))]
  public class CharacterMovement : MonoBehaviour
  {
    private CharacterController charController;
    public FloatReference MaxSpeed;

    [SerializeField]
    private Vector3 _moveDir;
    private Vector3 moveDir
    {
      get => _moveDir;
      set
      {
        _moveDir = Vector3.ClampMagnitude(value, 1);
      }
    }
    public Vector3 PlanarVelocity => moveDir.magnitude > 0 ? Vector3.ProjectOnPlane(moveDir, Vector3.up) : Vector3.zero;
    public float PlanarSpeed => PlanarVelocity.magnitude;
    public float ForwardSpeed => Mathf.Clamp01(Vector3.Dot(model.forward, PlanarVelocity));
    public float LateralSpeed => Vector3.Dot(PlanarVelocity, model.right);

    public float RotationSpeedY => Mathf.DeltaAngle(model.rotation.eulerAngles.y, lastYRotation) / Time.deltaTime / MovementRotationSpeed.Value;

    public float lastYRotation;

    //TODO:
    [FormerlySerializedAs("RotationSpeed")]
    public FloatReference MovementRotationSpeed;
    [FormerlySerializedAs("Rotate")]
    public bool RotateTowardsMovement = true;

    public Transform model;

    // Start is called before the first frame update
    private void Awake()
    {
      charController = GetComponent<CharacterController>();
      if (model == null)
      {
        model = transform.GetChild(0);
      }
    }

    public const float MIN_SPEED_THRESHOLD = 0.02f;

    // Update is called once per frame
    void Update()
    {
      if (RotateTowardsMovement)
      {
        if (PlanarSpeed > 0)
        {
          LookDirection(moveDir);
        }
      }

      charController.SimpleMove(ForwardSpeed * model.forward * MaxSpeed);
    }

    private void LateUpdate()
    {
      lastYRotation = model.rotation.eulerAngles.y;
    }

    private bool lockExternalMovement;

    public void Move(Vector3 moveDir)
    {
      if (lockExternalMovement) return;
      this.moveDir = Vector3.ClampMagnitude(moveDir, 1);
    }

    private void LookDirection(Vector3 direction)
    {
      if (MovementRotationSpeed == 0)
      {
        model.rotation = Quaternion.LookRotation(direction, Vector3.up);
      }
      else
      {
        model.rotation = Quaternion.RotateTowards(
            model.rotation,
            Quaternion.LookRotation(direction, Vector3.up),
            Time.deltaTime * MovementRotationSpeed);
      }
    }

    public IEnumerator MoveTo(Transform target, float speed = 1)
    {
      Vector3 getDirection() => Vector3.ProjectOnPlane(target.position - transform.position, Vector3.up);
      var initialDirection = getDirection();

      lockExternalMovement = true;
      for (var direction = initialDirection; Vector3.Dot(initialDirection, direction) > 0 && direction.magnitude >= MIN_SPEED_THRESHOLD; direction = getDirection())
      {
        this.moveDir = direction.normalized * speed;
        yield return null;
      }
      lockExternalMovement = false;

      Move(Vector3.zero);
    }

    public IEnumerator LookAt(Transform target, float time)
    {
      Vector3 getDirection() => Vector3.ProjectOnPlane(target.position - transform.position, Vector3.up);
      var targetRotation = Quaternion.LookRotation(getDirection(), Vector3.up);
      var initialRotation = model.rotation;

      yield return CoroutineHelpers.InterpolateByTime(time, (k) =>
      {
        model.rotation = Quaternion.Slerp(initialRotation, targetRotation, k);
      });
    }

    public IEnumerator LookAt(Transform target)
    {
      Vector3 getDirection() => Vector3.ProjectOnPlane(target.position - transform.position, Vector3.up);
      var initialDirection = getDirection();
      var initialRotation = Quaternion.Inverse(model.rotation) * Quaternion.LookRotation(getDirection(), Vector3.up);
      initialRotation.ToAngleAxis(out float _, out Vector3 initialAxis);

      var axis = initialAxis;
      var angle = Mathf.PI;
      while (Vector3.Dot(initialAxis, axis) > 0 && Mathf.Abs(angle) >= 0.01f)
      {
        LookDirection(initialDirection);
        var rotation = Quaternion.Inverse(model.rotation) * Quaternion.LookRotation(getDirection(), Vector3.up);
        rotation.ToAngleAxis(out angle, out axis);

        yield return null;
      }
    }
  }
}