﻿using System.Collections;
using System.Collections.Generic;
using UnityAtoms;
using UnityEngine;
using UnityEngine.UI;

namespace MutNarrativas
{
  [ExecuteInEditMode]
  public class DialogueOptionsContainer : MonoBehaviour
  {
    public IntVariable OptionSelection;
    public StringList DialogueOptions;

    private void Awake()
    {
      OptionSelection.SetValue(-1);
    }

    [SerializeField]
    private List<Button> DialogueOptionsButtons = new List<Button>();
    public Button ButtonPrefab;
    public void OnDialogueOptionsChange()
    {
      while (DialogueOptions.Count > DialogueOptionsButtons.Count)
      {
        var button = (Button)Instantiate(ButtonPrefab, transform);
        var capture = DialogueOptionsButtons.Count;
        button.onClick.AddListener(() => SelectOption(capture));

        DialogueOptionsButtons.Add(button);
      }

      for (int i = 0; i < DialogueOptionsButtons.Count; i++)
      {
        var button = DialogueOptionsButtons[i];
        if (i < DialogueOptions.Count)
        {
          button.GetComponentInChildren<TMPro.TMP_Text>().text = DialogueOptions[i];
          button.gameObject.SetActive(true);
        }
        else
        {
          button.gameObject.SetActive(false);
        }
      }
      void SelectOption(int i)
      {
        UberDebug.LogChannel("mutnarrativas", $"Selected option {i}");
        OptionSelection.SetValue(i);
        OptionSelection.SetValue(-1);
      }
    }
  }
}