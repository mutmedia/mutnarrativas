using System;

namespace UnityAtoms
{

  public static class UnityAtomsExtensions
  {
    public static void RegisterAction<T>(this AtomEvent<T> ev, Action<T> action)
    {
      ev.RegisterListener(new ActionEventListener<T>(action));
    }

    public static void RegisterActionOneShot<T>(this AtomEvent<T> ev, Action<T> action)
    {
      var cb = new ActionEventListener<T>();
      cb = new ActionEventListener<T>((t) =>
     {
       action(t);
       ev.UnregisterListener(cb);
     });
      ev.RegisterListener(cb);
    }
  }
}