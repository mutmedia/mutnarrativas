using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace MutModels
{
  [CreateAssetMenu(fileName = "MutModelsSettingsDefault", menuName = "MutModels/Settings", order = 1)]
  public class MutModelsSettings : ScriptableObject
  {
#if UNITY_EDITOR
    static Lazy<MutModelsSettings> LazySettings = new Lazy<MutModelsSettings>(() =>
        {
          var result = AssetDatabase.FindAssets("MutModelsSettingsDefault");
          var path = AssetDatabase.GUIDToAssetPath(result[0]);
          return AssetDatabase.LoadAssetAtPath<MutModelsSettings>(path);
        });

    public static MutModelsSettings S => LazySettings.Value;

#endif
    public Material VertexLitMaterial;
    //TODO: make all folders named mixamo do this
    public string VertexColorFolder = "Kosha";
    public string MixamoFolder = "Mixamo";

    [Serializable]
    public struct ModelAvatarPair
    {
      public string Name;
      public Avatar Avatar;
    }

    public List<ModelAvatarPair> SourceAvatars;
  }
}