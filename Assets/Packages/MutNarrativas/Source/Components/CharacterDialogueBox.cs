﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityAtoms;
using UnityEngine;
using UnityEngine.UI;

namespace MutNarrativas
{
  [RequireComponent(typeof(StringListener))]
  public class CharacterDialogueBox : MonoBehaviour
  {
#pragma warning disable CS0649
    [SerializeField]
    private TMPro.TMP_Text text;
#pragma warning restore CS0649

    public StringVariable TextVariable;

    public void BindInteractable(Interactable interactable)
    {
      // TODO: make this better
      var worldCanvas = GameObject.FindGameObjectWithTag("WorldCanvas");
      if (worldCanvas == null)
      {
        UberDebug.LogErrorChannel("mutnarrativas", "No WorldCanvas object found");
      }

      TextVariable = interactable.Text;

      this.name = $"{interactable.Name}TextBox";

      BoundGameObject.Bind(interactable.gameObject, this.gameObject, () =>
      {
        this.transform.SetParent(worldCanvas.transform, false);
      });

      var transformFollower = this.GetComponent<TransformFollower>();
      if (transformFollower != null)
      {
        transformFollower.ThingToFollow = interactable.VisualRepresentation;
      }

      var characterText = interactable.Text;
      if (characterText == null)
      {
        characterText = ScriptableObject.CreateInstance<StringVariable>();
        characterText.name = interactable.Name;
        UberDebug.LogErrorChannel("mutnarrativas", $"Could not find Character Text for {interactable.Name}, using in game instantiation");
      }
    }

    private void Start()
    {
      var listener = GetComponent<StringListener>();
      listener.Event = TextVariable.Changed;
      //FIXME: super hack
      listener.enabled = false;
      listener.enabled = true;
      Hide();
    }

    public void Hide()
    {
      foreach (Transform c in transform)
      {
        c.gameObject.SetActive(false);
      }
    }

    public void Show()
    {
      foreach (Transform c in transform)
      {
        c.gameObject.SetActive(true);
      }
    }

    public void OnTextChange(string item)
    {
      if (item == "")
      {
        Hide();
      }
      else
      {
        Show();
      }
      text.text = item;
    }
  }
}