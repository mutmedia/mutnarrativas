// TODO: move to own file
using System;

namespace UnityAtoms
{
  public class ActionEventListener<T> : IAtomListener<T>
  {
    private Action<T> action;
    public void OnEventRaised(T item)
    {
      action?.Invoke(item);
    }

    public ActionEventListener()
    {
    }

    public ActionEventListener(Action<T> action)
    {
      this.action = action;
    }
  }
}