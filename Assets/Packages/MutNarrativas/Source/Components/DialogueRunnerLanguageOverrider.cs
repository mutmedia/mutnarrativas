using System;
using UnityAtoms;
using UnityEngine;
using Yarn.Unity;

namespace MutNarrativas
{
  [RequireComponent(typeof(DialogueRunner))]
  public class DialogueRunnerLanguageOverrider : MonoBehaviour
  {
    [SerializeField]
    private StringVariable language;

    private DialogueRunner runner;

    // Start is called before the first frame update
    void Awake()
    {
      runner = GetComponent<DialogueRunner>();
      var setOverrideLanguage = SetOverrideLanguage(runner);
      setOverrideLanguage(language.Value);
    }

    public static Action<string> SetOverrideLanguage(DialogueRunner runner)
      => (string language)
      => runner.overrideLanguage
                  = (SystemLanguage)Enum.Parse(typeof(SystemLanguage), language, true);
  }
}