﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;

namespace MutModels
{
  public class KoshaModelProcessor : AssetPostprocessor
  {
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    bool IsInVertexColorDirectory => false; //assetPath.ToLower().IndexOf(MutModelsSettings.S.VertexColorFolder.ToLower()) != -1;
    bool IsInMixamoDirectory => assetPath.ToLower().IndexOf(MutModelsSettings.S.MixamoFolder.ToLower()) != -1;

    void OnPreprocessModel()
    {
      var modelImporter = (ModelImporter)assetImporter;
      if (IsInVertexColorDirectory)
      {
        UberDebug.LogChannel("mutmodels", "Preprocessing vertex color");
        modelImporter.importMaterials = false;
      }
      if (IsInMixamoDirectory)
      {
        UberDebug.LogChannel("mutmodels", "Preprocessing mixamo animations");
        modelImporter.importAnimation = true;
        modelImporter.generateAnimations = ModelImporterGenerateAnimations.GenerateAnimations;
        modelImporter.animationType = ModelImporterAnimationType.Human;
      }
    }

    // TODO:
    //void OnPostProcessAnimation()

    void OnPostprocessModel(GameObject g)
    {
      if (!IsInMixamoDirectory) return;
      UberDebug.LogChannel("mutmodels", "Importing character mixamo animations");
      if (!g.name.Contains('@'))
      {
        UberDebug.LogWarningChannel("mutmodels", "Character Name should have and @ and the animation name");
        return;
      }
      var isAvatar = g.name.ToLower().Contains("t-pose");
      var name = string.Join("", g.name.TakeWhile(c => c != '@'));
      var path = string.Join("", assetPath.TakeWhile(c => c != '@'));

      var modelImporter = (ModelImporter)assetImporter;

      modelImporter.animationType = ModelImporterAnimationType.Human;

      // Animations
      if (!isAvatar)
      {
        var a = AssetDatabase.LoadAssetAtPath<Animator>(path + "@t-pose.fbx");
        if (a != null)
        {
          modelImporter.sourceAvatar = a.avatar;
        }
        else
        {
          modelImporter.SaveAndReimport();
        }
      }

      var animationClips = modelImporter.defaultClipAnimations;
      for (int i = 0; i < animationClips.Length; i++)
      {
        var animationClip = animationClips[i];
        animationClip.loop = true;
        animationClip.loopTime = true;
        animationClip.loopPose = true;

        animationClip.lockRootRotation = true;
        animationClip.lockRootHeightY = true;
        animationClip.lockRootPositionXZ = true;
        animationClip.name = name + "@" + animationClip.name;
      }
      modelImporter.clipAnimations = animationClips;
    }

    //Material OnAssignMaterialModel(Material material, Renderer renderer)
    //{
    //  if (!IsInVertexColorDirectory) return null;
    //  UberDebug.LogChannel("mutmodels", "Applying vertex color");
    //  material = MutModelsSettings.S.VertexLitMaterial;
    //  return material;
    //}
  }
}