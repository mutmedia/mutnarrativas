using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Yarn.Unity;
using System.Linq;
using UnityAtoms;

namespace MutNarrativas.Player
{
  [RequireComponent(typeof(Collider))]
  public class PlayerInteraction : MonoBehaviour
  {
    [SerializeField] private float interactionRadius = 2.0f;
    
    [SerializeField] private KeyCode interactionKey = KeyCode.Space;
    
    [SerializeField] private BoolVariable canInteract;
    
    [SerializeField] private VoidEvent onPlayerInteract;

    private Collider collider;

    /// Draw the range at which we'll start talking to people.
    void OnDrawGizmosSelected()
    {
      Gizmos.color = Color.green;

      // Need to draw at position zero because we set position in the line above
      Gizmos.DrawWireSphere(transform.position, interactionRadius);
    }

    /// Update is called once per frame
    void Update()
    {
      // TODO: make this only on blocking dialogue
      // Remove all player control when we're in dialogue
      if (FindObjectOfType<DialogueRunner>().isDialogueRunning == true)
      {
        return;
      }
        
      var target = GetNearestInteractable();
      canInteract.Value = target != null;
      
      if (Input.GetKeyDown(interactionKey))
      {
        onPlayerInteract?.Raise();
        target.StartDialogue();
      }
    }

    /// Find all DialogueParticipants
    /** Filter them to those that have a Yarn start node and are in range; 
     * then start a conversation with the first one
     */
    //TODO: make this collider
    public Interactable GetNearestInteractable()
    {
      var interactables = new List<Interactable>(FindObjectsOfType<Interactable>()).Where(p => p.gameObject != gameObject);
      if (interactables.Count() > 0)
      {
        // Get closest one
        var target = interactables.Aggregate((min, t) => (min == null || Vector3.Distance(t.transform.position, transform.position) < Vector3.Distance(min.transform.position, transform.position) ? t : min));
        if (Vector3.Distance(target.transform.position, transform.position) < interactionRadius)
        {
          if (target != null)
          {
            // Kick off the dialogue at this node.
            return target;
          }
        }
      }
      
      return null;
    }
  }
}