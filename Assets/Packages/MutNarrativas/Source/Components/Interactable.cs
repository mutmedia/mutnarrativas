using UnityEngine;
using System.Collections;
using UnityEngine.Serialization;
using UnityAtoms;
using System.Collections.Generic;
using Yarn.Unity;
using System;

namespace MutNarrativas
{
  [SelectionBase]
  public class Interactable : MonoBehaviour
  {
    //TODO: transformar em um set de scriptable object por cena
    public static Dictionary<string, Interactable> all = new Dictionary<string, Interactable>();

    public static bool TryFind(string name, out Interactable interactable)
    {
      return all.TryGetValue(name.ToLower(), out interactable);
    }

    [SerializeField]
    private InteractableModel model;

    public StringVariable Text => model.Text;
    public bool CanInteract => model.CanInteract;
    public string TalkToNode => model.TalkToNode;
    internal string Name => model.Name;
    public CharacterDialogueBox DialogueBox => model.DialogueBox;
    public Transform VisualRepresentation => model.VisualRepresentation.transform;

#pragma warning disable CS0649
    [SerializeField]
    public VoidEvent OnAnyDialogueStarted;

    [SerializeField]
    public VoidEvent OnAnyDialogueComplete;
#pragma warning restore CS0649

    public VoidUnityEvent OnInteractionComplete;
    public VoidUnityEvent OnInteractionStarted;

    private DialogueRunner _dialogueRunner;

    private DialogueRunner dialogueRunner => _dialogueRunner = _dialogueRunner ?? FindObjectOfType<DialogueRunner>();


    public static Interactable Create(string name, List<TextAsset> scriptsToLoad, bool interactable, string customTalkToNode = "")
    {
      var dc = new Interactable();
      return dc;
    }

#if UNITY_EDITOR
    public void Setup(InteractableModel model)
    {
      this.model = model;
      //OnAnyDialogueComplete = MutNarrativasEditorSettings.S.OnDialogueComplete;
      //OnAnyDialogueStarted = MutNarrativasEditorSettings.S.OnDialogueStarted;
    }

    void OnValidate()
    {
      if (model.VisualRepresentation == null && transform.childCount > 0)
      {
        model.VisualRepresentation = transform.GetChild(0).gameObject;
      }

      /* 
      if (model.Text == null)
      {
        var resourcePath = MutDialogosSettings.S.CharacterTextLocation(model.Name);
        model.Text = Resources.Load<StringVariable>(resourcePath);
        print("loaded character text" + model.Text);
      }*/

      //if (model.Name.ToLower() != name.ToLower())
      //{
      //Debug.LogWarning($"Character text name {model.Name} is not the same as gameobject name {name}");
      //}
    }
#endif

    // Use this for initialization
    void Awake()
    {
      if (model.Name == "")
      {
        model.Name = name;
      }

      var dialogueBox = Instantiate<CharacterDialogueBox>(model.DialogueBox);
      dialogueBox.BindInteractable(this);

      if (model.IsUnique)
      {
        if (all.ContainsKey(model.Name.ToLower()))
        {
          UberDebug.LogWarningChannel("mutnarrativas", $"Duplicate Character: {model.Name}");
        }
        else
        {
          all.Add(model.Name.ToLower(), this);
          //Debug.Log($"Added {data.CharacterName} to characters list.");
        }
      }

      Text.Value = "";

      if (model.ScriptToLoad != null)
      {
        try
        {
          dialogueRunner.AddScript(model.ScriptToLoad);
        }
        catch (InvalidOperationException e)
        {
          if (model.IsUnique)
          {
            UberDebug.LogWarningChannel("mutnarrativas", "Duplicate loading: " + e.Message);
          }
        }
      }
    }


    public void StartDialogue()
    {
      OnInteractionStarted.Invoke(new UnityAtoms.Void());
      OnAnyDialogueComplete.RegisterActionOneShot(OnInteractionComplete.Invoke);
      UberDebug.LogChannel("MutNarrativas", $"Starting Dialogue with Interactable {Name}");
      dialogueRunner.StartDialogue(TalkToNode);
    }
  }
}
