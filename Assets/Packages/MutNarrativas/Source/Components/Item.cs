﻿using System.Collections;
using System.Collections.Generic;
using UnityAtoms;
using UnityEngine;

namespace MutNarrativas
{
  public class Item : MonoBehaviour
  {
    public IntReference item;

    public void Destroy()
    {
      Destroy(this.gameObject);
    }
  }
}