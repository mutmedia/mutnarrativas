using UnityAtoms;
using UnityEngine;
/// An extremely simple implementation of DialogueUnityVariableStorage, which
/// just stores everything in a Dictionary.
namespace MutNarrativas
{
  [CreateAssetMenu(menuName = "MutNarrativas/DialogueVariable/Event", fileName = "DialogueVariableEvent", order = 1)]
  public class DialogueVariableEvent : AtomEvent<DialogueVariable>
  {
  }
}