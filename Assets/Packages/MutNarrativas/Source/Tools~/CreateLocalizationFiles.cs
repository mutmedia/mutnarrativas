﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.IO;
using MutCommon;
using System.Linq;
using System.Diagnostics;

namespace MutNarrativas.Tools
{
  public class CreateLocalizationFiles : EditorWindow
  {
    public IEnumerable<string> yarnFiles;
    public IEnumerable<string> localizedYarnFiles;
    public IEnumerable<string> nonLocalizedYarnFiles;
    public IEnumerable<string> localizationFiles;

    [MenuItem("MutNarrativas/Localization/Generate")]
    private static void ShowWindow()
    {
      var window = GetWindow<CreateLocalizationFiles>();
      window.titleContent = new GUIContent("CreateLocalizationFiles");
      window.Refresh();
      window.Show();
    }

    private static IEnumerable<string> YarnFiles =>
      Directory.GetFiles(Application.dataPath, "*.yarn.txt", SearchOption.AllDirectories);

    private static IEnumerable<string> LocalizationFiles =>
      Directory.GetFiles(Application.dataPath, "*.yarn_lines.csv", SearchOption.AllDirectories);

    private static IEnumerable<string> LocalizedYarnFiles =>
      YarnFiles.Where(filePath =>
      {
        var fileName = Path.GetFileName(filePath);
        var fileNameWithoutExtension = fileName.Split('.')[0];
        return LocalizationFiles.Any(f => Path.GetFileName(f).Split('.')[0] == fileNameWithoutExtension);
      });

    private static IEnumerable<string> NonLocalizedYarnFiles => YarnFiles.Where(f => !LocalizedYarnFiles.Contains(f));

    void Refresh()
    {
      yarnFiles = YarnFiles;
      localizedYarnFiles = LocalizedYarnFiles;
      nonLocalizedYarnFiles = NonLocalizedYarnFiles;
      localizationFiles = LocalizationFiles;
    }

    private void OnGUI()
    {
      EditorGUILayout.LabelField("Yarn Files to localize:", EditorStyles.boldLabel);
      foreach (var file in nonLocalizedYarnFiles)
      {
        EditorGUILayout.LabelField(Path.GetFileNameWithoutExtension(file));
      }

      EditorGUILayout.Separator();

      EditorGUILayout.LabelField("Yarn Files already localized:", EditorStyles.boldLabel);
      foreach (var file in localizedYarnFiles)
      {
        EditorGUILayout.LabelField(Path.GetFileNameWithoutExtension(file));
      }

      EditorGUILayout.Separator();

      EditorGUILayout.LabelField("Localization Files:", EditorStyles.boldLabel);
      foreach (var file in localizationFiles)
      {
        EditorGUILayout.LabelField(Path.GetFileNameWithoutExtension(file));
      }

      EditorGUILayout.Separator();


      if (GUILayout.Button("Refresh"))
        Refresh();
      if (GUILayout.Button("Create"))
      {
        AssetDatabaseHelper.CreateFolderIfNotExists(MutNarrativasSettings.S.LocalizationPath);

        var path = Path.Combine(Application.dataPath, "..", MutNarrativasSettings.S.LocalizationPath);
        var yarnSpinnerConsoleExe = Directory
          .GetFiles(Application.dataPath, "YarnSpinnerConsole.exe", SearchOption.AllDirectories)[0];

        Process yarnSpinnerConsole = new Process();
        yarnSpinnerConsole.StartInfo.WorkingDirectory = path;
        yarnSpinnerConsole.StartInfo.FileName = yarnSpinnerConsoleExe;
        yarnSpinnerConsole.StartInfo.Arguments = "taglines " + yarnFiles.Aggregate((a, c) => a + " " + c);
        yarnSpinnerConsole.Start();

        yarnSpinnerConsole.StartInfo.Arguments = "genstrings " + yarnFiles.Aggregate((a, c) => a + " " + c);
        yarnSpinnerConsole.Start();

        foreach (var filePath in localizationFiles)
        {
          var fileName = Path.GetFileName(filePath);
          var targetPath = Path.Combine(path, fileName);
          File.Move(filePath, targetPath);
        }

        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
      }
    }
  }
}