﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MutNarrativas
{
  public class Crossfader : MonoBehaviour
  {
    public MusicFader faderPrefab;

    MusicFader fader0;
    MusicFader fader1;

    [SerializeField]
    private AudioClip currentClip;

    int activeFader = 0;



    // Start is called before the first frame update
    void Start()
    {
      fader0 = Instantiate(faderPrefab, this.transform);
      fader1 = Instantiate(faderPrefab, this.transform);
    }

    public void Crossfade(AudioClip song, float time)
    {
      var (current, next) = activeFader == 0 ? (fader0, fader1) : (fader1, fader0);

      next.Source.clip = song;
      current.FadeOut(time);
      next.FadeIn(time);

      activeFader = (activeFader + 1) % 2;
    }
  }
}