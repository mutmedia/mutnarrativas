using System;
using System.Collections;
using MutPersonagens;
using UnityEngine;

namespace MutNarrativas.Commands
{
  internal static class PlayerCommands
  {
    internal static IEnumerator PlayerLock(string[] parameters)
    {
      (PlayerController player, bool error) = TryGetPlayer(parameters);
      if (error)
        yield break;

      player.Lock();
    }

    internal static IEnumerator PlayerUnlock(string[] parameters)
    {
      (PlayerController player, bool error) = TryGetPlayer(parameters);
      if (error)
        yield break;

      player.Unlock();
    }

    private static (PlayerController player, bool error) TryGetPlayer(string[] parameters)
    {
      if (parameters.Length > 1)
      {
        UberDebug.LogErrorChannel("mutnarrativas", "The Locking command is with incorrect parameters");
        return (null, true);
      }

      var playerCharacterGO = GameObject.Find(parameters[0]);
      if (playerCharacterGO == null)
      {
        UberDebug.LogErrorChannel("mutnarrativas", $"There is no character with name {parameters[0]}");
        return (null, true);
      }

      var playerCharacter = playerCharacterGO.GetComponent<PlayerController>();

      if (playerCharacter == null)
      {
        UberDebug.LogErrorChannel("mutnarrativas", $"There is no player character with name {parameters[0]}");
        return (null, true);
      }

      return (playerCharacter, false);
    }

  }
}