namespace MutPersonagens
{
  public enum CharacterEmotion
  {
    Neutral,
    Happy,
    Sad,
  }
}