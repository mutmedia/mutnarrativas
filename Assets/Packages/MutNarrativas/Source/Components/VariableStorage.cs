﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Yarn.Unity;
using UnityAtoms;
using System.Linq;
using UnityEngine.Serialization;

/// An extremely simple implementation of DialogueUnityVariableStorage, which
/// just stores everything in a Dictionary.
namespace MutNarrativas
{
  public class VariableStorage : VariableStorageBehaviour
  {
    public bool debug;

    /// Where we actually keeping our variables
    // TODO: use actual dictionary? Create VariableDictionary for atoms?
    //Dictionary<string, DialogueVariable> variables = new Dictionary<string, DialogueVariable>();

    /// Our list of default variables, for debugging.
    // TODO: use DialogueVariableList
    public DialogueVariableList defaultVariables;

    public DialogueVariableList variables;

    /// Reset to our default values when the game starts
    void Awake()
    {
      ResetToDefaults();
    }

    /// Erase all variables and reset to default values
    public override void ResetToDefaults()
    {
      Clear();

      // For each default variable that's been defined, parse the string
      // that the user typed in in Unity and store the variable
      foreach (var variable in defaultVariables)
      {

        object value;

        switch (variable.Type)
        {
          case Yarn.Value.Type.Number:
            float f = 0.0f;
            float.TryParse(variable.Value, out f);
            value = f;
            break;

          case Yarn.Value.Type.String:
            value = variable.Value;
            break;

          case Yarn.Value.Type.Bool:
            bool b = false;
            bool.TryParse(variable.Value, out b);
            value = b;
            break;

          case Yarn.Value.Type.Variable:
            // We don't support assigning default variables from other variables
            // yet
            UberDebug.LogErrorChannel("mutnarrativas", $"Can't set variable {variable.Name} to {variable.Value}: You can't " +
                "set a default variable to be another variable, because it " +
                "may not have been initialised yet.");
            continue;

          case Yarn.Value.Type.Null:
            value = null;
            break;

          default:
            throw new System.ArgumentOutOfRangeException();

        }

        var v = new Yarn.Value(value);

        variables.Add(new DialogueVariable()
        {
          Name = variable.Name,
          Variable = variable.Variable,
          Type = variable.Type,
        });

        //SetValue(variable.Name, v);
      }
    }

    /// Set a variable's value
    public override void SetValue(string variableName, Yarn.Value value)
    {
      // Copy this value into our list
      variableName = variableName.Substring(1);
      var variable = variables.FirstOrDefault((v) => v.Name == variableName);
      if (variable == null)
      {
        //print("Creating variable " + variableName);
        var stringVar = ScriptableObject.CreateInstance<StringVariable>();
        variable = new DialogueVariable()
        {
          Name = variableName,
          Variable = stringVar,
          Type = value.type,
        };
        variables.Add(variable);
      }

      variable.Value = value.AsString;
    }

    /// Get a variable's value
    public override Yarn.Value GetValue(string variableName)
    {
      // If we don't have a variable with this name, return the null value
      variableName = variableName.Substring(1);
      var variable = variables.FirstOrDefault(v => v.Name == variableName);
      if (variable == null)
        return Yarn.Value.NULL;

      switch (variable.Type)
      {
        case Yarn.Value.Type.Number:
          float f = 0.0f;
          float.TryParse(variable.Value, out f);
          return new Yarn.Value(f);

        case Yarn.Value.Type.String:
          return new Yarn.Value(variable.Value);

        case Yarn.Value.Type.Bool:
          bool b = false;
          bool.TryParse(variable.Value, out b);
          return new Yarn.Value(b);

        case Yarn.Value.Type.Null:
          return Yarn.Value.NULL;

        case Yarn.Value.Type.Variable:
          UberDebug.LogErrorChannel("mutnarrativas", $"Can't set variable {variable.Name} to {variable.Value}: You can't " +
              "set a default variable to be another variable, because it " +
              "may not have been initialised yet.");
          return Yarn.Value.NULL;
        default:
          throw new System.ArgumentOutOfRangeException();

      }
    }

    /// Erase all variables
    public override void Clear()
    {
      variables.Clear();
    }
  }
}