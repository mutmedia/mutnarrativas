﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Yarn.Unity;
using System.Linq;

namespace MutPersonagens
{
  [RequireComponent(typeof(CharacterMovement))]
  public class PlayerController : MonoBehaviour
  {

    private CharacterMovement characterMovement;

    public bool LockAtStart;

    private int lockValue = 0;
    private bool isLocked
    {
      get
      {
        return lockValue != 0;
      }
      set
      {
        if (value)
        {
          lockValue++;
        }
        else
        {
          lockValue--;
        }
        lockValue = Mathf.Clamp(lockValue, 0, 2);
      }
    }

    public void Lock()
    {
      isLocked = true;
      characterMovement.Move(Vector3.zero);
    }


    public void Unlock() => isLocked = false;

    private void Awake()
    {
      characterMovement = GetComponent<CharacterMovement>();
      if (LockAtStart)
      {
        isLocked = true;
      }
    }

    /// Update is called once per frame
    void Update()
    {
      var moveDir = Vector3.zero;
      if (!isLocked)
      {
        var camDir = transform.position - Camera.main.transform.position;
        var camForward = Vector3.ProjectOnPlane(camDir, Vector3.up);
        if (camForward.magnitude < 0.001f)
        {
          camForward = Camera.main.transform.up;
        }
        else
        {
          camForward = camForward.normalized;
        }
        var camRight = Camera.main.transform.right;
        moveDir = camForward * Input.GetAxis("Vertical") +
                  camRight * Input.GetAxis("Horizontal");
        var speedMult = Input.GetKey(KeyCode.LeftShift) ? 1.0f : 0.5f;
        characterMovement.Move(moveDir.normalized * speedMult);
      }
    }
  }
}