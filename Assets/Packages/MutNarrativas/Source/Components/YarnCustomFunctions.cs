using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Yarn;
using Yarn.Unity;

namespace MutNarrativas
{
  [RequireComponent(typeof(Yarn.Unity.DialogueRunner))]
  public class YarnCustomFunctions : MonoBehaviour
  {
    void Awake()
    {
      var dialogueRunner = GetComponent<Yarn.Unity.DialogueRunner>();
      RegisterFunctions(dialogueRunner.dialogue);
    }

    public static void RegisterFunctions(Dialogue d)
    {
      d.library.RegisterFunction("currentScene", 0, (ReturningFunction)((_) => SceneManager.GetActiveScene().name));
    }
  }
}