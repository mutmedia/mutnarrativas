using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace MutPersonagens
{
  [CreateAssetMenu(fileName = "MutPersonagensSettingsDefault", menuName = "MutPersonagens/Settings", order = 1)]
  public class MutPersonagensSettings : ScriptableObject
  {
#if UNITY_EDITOR
    static Lazy<MutPersonagensSettings> LazySettings = new Lazy<MutPersonagensSettings>(() =>
        {
          var result = AssetDatabase.FindAssets("MutPersonagensSettingsDefault");
          var path = AssetDatabase.GUIDToAssetPath(result[0]);
          return AssetDatabase.LoadAssetAtPath<MutPersonagensSettings>(path);
        });

    public static MutPersonagensSettings S => LazySettings.Value;

#endif
    public RuntimeAnimatorController BaseAnimator;
  }
}