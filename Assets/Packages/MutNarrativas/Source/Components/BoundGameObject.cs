﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace MutNarrativas
{

  [ExecuteInEditMode]
  public class BoundGameObject : MonoBehaviour
  {
    Action OnAwake = () => { };
    public static void Bind(GameObject a, GameObject b) => Bind(a, b, () => { });
    public static void Bind(GameObject bindA, GameObject bindB, Action onAwake)
    {
      var bindAB = bindA.AddComponent<BoundGameObject>();
      bindAB.bound = bindB;
      bindAB.hideFlags = HideFlags.HideInInspector;
      bindAB.OnAwake = onAwake;
      //bindBA.hideFlags = HideFlags.HideInInspector;
    }

    public GameObject bound;

    private void Start()
    {
      OnAwake();
    }

    private void OnDestroy()
    {
      DestroyImmediate(bound);
    }
  }
}