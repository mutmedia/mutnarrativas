using System;
using System.Collections;
using System.Linq;
using MutPersonagens;
using UnityEngine;

namespace MutNarrativas.Commands
{
  internal static class CharacterCommands
  {
    internal static IEnumerator LookatPosition(string[] parameters)
    {
      UberDebug.LogChannel("mutnarrativas", "Starting the lookat command");
      if (parameters.Count() != 2 && parameters.Count() != 3)
      {
        UberDebug.LogErrorChannel("mutnarrativas", "The lookat command is with incorrect parameters");
        yield break;
      }
      if (Interactable.TryFind(parameters[0], out Interactable interactable))
      {
        var characterMover = interactable.GetComponent<CharacterMovement>();
        if (characterMover == null)
        {
          UberDebug.LogErrorChannel("mutnarrativas", "The object to lookat does not have a movement script");
          yield break;
        }

        var target = GameObject.Find(parameters[1]);
        if (target == null)
        {
          UberDebug.LogErrorChannel("mutnarrativas", "There is no gameobject with the target name: " + parameters[1]);
          yield break;
        }

        if (parameters.Count() == 3)
        {
          if (float.TryParse(parameters[2], out float time))
          {
            yield return characterMover.LookAt(target.transform, time);
          }
          else
          {

            UberDebug.LogErrorChannel("mutnarrativas", "Defined time is not a number");
            yield break;
          }
        }
        else
        {
          yield return characterMover.LookAt(target.transform);
        }

      }
      UberDebug.LogChannel("mutnarrativas", "Ending the lookat command");
    }

    internal static IEnumerator MoveToPosition(string[] parameters)
    {
      UberDebug.LogChannel("mutnarrativas", "Starting the move command");
      if (parameters.Count() != 3)
      {
        UberDebug.LogErrorChannel("mutnarrativas", "The move command is with incorrect parameters");
        yield break;
      }

      var characterName = parameters[0];
      var targetName = parameters[1];
      var speedString = parameters[2];

      if (Interactable.TryFind(characterName, out Interactable interactable))
      {
        var characterMover = interactable.GetComponent<CharacterMovement>();
        if (characterMover == null)
        {
          UberDebug.LogErrorChannel("mutnarrativas", "The object to move does not have a movement script");
          yield break;
        }

        var target = GameObject.Find(targetName);
        if (target == null)
        {
          UberDebug.LogErrorChannel("mutnarrativas", "There is no gameobject with the target name: " + targetName);
          yield break;
        }

        if (!float.TryParse(speedString, out float speed))
        {
          UberDebug.LogErrorChannel("mutnarrativas", "Defined speed is not a number");
          yield break;
        }

        var clampedSpeed = Mathf.Clamp(speed, 0.0f, 100.0f);
        if (clampedSpeed != speed)
        {
          UberDebug.LogWarningChannel("mutnarrativas", "Defined speed is not between 0 and 100, clamping");
        }

        yield return characterMover.MoveTo(target.transform, clampedSpeed / 100.0f);
      }
      UberDebug.LogChannel("mutnarrativas", "Ending the move command");
    }

    internal static CommandExecution MoveToPositionWithSpeed(float speed)
    {
      return (string[] parameters) =>
      {
        var extendedParameters = parameters.Append(speed.ToString()).ToArray();
        return MoveToPosition(extendedParameters);
      };
    }

    internal static IEnumerator ChangeEmotion(string[] parameters)
    {
      if (parameters.Count() != 2)
      {
        UberDebug.LogErrorChannel("mutnarrativas", "The emotion command is with incorrect parameters");
        yield break;
      }

      var characterName = parameters[0];
      var emotionName = parameters[1];

      var character = GameObject.Find(characterName);
      if (character == null)
      {
        UberDebug.LogErrorChannel("mutnarrativas", "There is no gameobject with the target name: " + character);
        yield break;
      }

      var characterAnimator = character.GetComponent<CharacterAnimator>();
      if (characterAnimator == null)
      {
        UberDebug.LogErrorChannel("mutnarrativas", "There is no gameobject with the target name: " + characterAnimator);
        yield break;
      }

      if (Enum.TryParse<CharacterEmotion>(emotionName, true, out CharacterEmotion emotion))
      {
        characterAnimator.SetEmotion(emotion);
      }
      else
      {
        UberDebug.LogErrorChannel("mutnarrativas", "There is no emotion with the name: " + emotionName);
        yield break;
      }
    }
  }
}