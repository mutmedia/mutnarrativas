using System;
using System.Linq;
using System.Collections;
using MutPersonagens;
using UnityAtoms;
using UnityEngine;

namespace MutNarrativas.Commands
{
  internal class EventCommandManager : MonoBehaviour
  {
    private static EventCommandManager instance;

    [SerializeField]
    private UnityAtoms.AtomEvent[] preloadedEvents;

    UnityAtoms.AtomEvent[] cachedEvents;

    void Awake()
    {
      // Singleton logic
      {
        if (instance != null && instance != this)
        {
          Destroy(this.gameObject);
          return;
        }
        else
        {
          instance = this;
        }

        transform.parent = null;
        DontDestroyOnLoad(this.gameObject);
      }

      var resourceEvents = Resources.LoadAll<AtomEvent>("Events");

      cachedEvents = preloadedEvents.Concat(resourceEvents).ToArray();

      DialogueSystemCommands.AddCommand("event", TriggerEvent);
    }

    internal IEnumerator TriggerEvent(string[] parameters)
    {
      if (parameters.Length != 1 && parameters.Length != 2)
      {
        UberDebug.LogErrorChannel("mutnarrativas", $"The Event command is with incorrect parameters {parameters.Length}");
        yield break;
      }

      var eventName = parameters[0];

      var eventTriggered = cachedEvents.FirstOrDefault(e => e.name.ToLower() == eventName.ToLower());

      if (eventTriggered == null)
      {
        UberDebug.LogErrorChannel("mutnarrativas", $"The Event named {eventName} does not exist.");
        yield break;
      }

      bool found = false;
      if (parameters.Length == 1)
      {
        if (eventTriggered is VoidEvent)
        {
          var voidEvent = eventTriggered as VoidEvent;
          voidEvent.Raise();
          found = true;
        }
      }
      else
      {
        var param = parameters[1];

        if (eventTriggered is StringEvent)
        {
          var stringEvent = eventTriggered as StringEvent;
          stringEvent.Raise(param);
          found = true;
        }
        else if (eventTriggered is IntEvent)
        {
          var ev = eventTriggered as IntEvent;
          if (int.TryParse(param, out int castedParam))
          {
            ev.Raise(castedParam);
            UberDebug.LogChannel("mutnarrativas", $"Could run event {eventName} with parameter \"{castedParam}\" given");
            found = true;
          }
          else
          {
            UberDebug.LogErrorChannel("mutnarrativas", $"Could not run event {eventName} of type int with parameter \"{param}\" given");
            yield break;
          }
        }
        else if (eventTriggered is FloatEvent)
        {
          var ev = eventTriggered as FloatEvent;
          if (float.TryParse(param, out float castedParam))
          {
            ev.Raise(castedParam);
            found = true;
          }
          else
          {
            UberDebug.LogErrorChannel("mutnarrativas", $"Could not run event {eventName} of type float with parameter \"{param}\" given");
            yield break;
          }
        }
        else if (eventTriggered is BoolEvent)
        {
          var ev = eventTriggered as BoolEvent;
          if (bool.TryParse(param, out bool castedParam))
          {
            ev.Raise(castedParam);
            found = true;
          }
          else
          {
            UberDebug.LogErrorChannel("mutnarrativas", $"Could not run event {eventName} of type bool with parameter \"{param}\" given");
            yield break;
          }
        }
      }

      if (!found)
      {
        UberDebug.LogErrorChannel("mutnarrativas", $"The Event named {eventName} with the current implemented type (Void, String, Int, Bool) could not be found with given parameters.");
        yield break;
      }
      yield return null;
    }
  }
}