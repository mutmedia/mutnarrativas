﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MutPersonagens
{
  [RequireComponent(typeof(CharacterMovement))]
  public class CharacterAnimator : MonoBehaviour
  {
    private Animator animator;
    private CharacterMovement charMovement;

    [SerializeField]
    private float rotationDamp;

    [SerializeField]
    private float forwardDamp;

#if UNITY_EDITOR
    private void OnValidate()
    {
      if (animator == null)
      {
        animator = GetComponentInChildren<Animator>();
      }
      if (animator != null && animator.runtimeAnimatorController == null)
      {
        animator.runtimeAnimatorController = MutPersonagensSettings.S.BaseAnimator;
      }

      if (charMovement == null)
      {
        charMovement = GetComponent<CharacterMovement>();
      }
    }
#endif

    void Awake()
    {
      if (animator == null)
      {
        animator = GetComponentInChildren<Animator>();
        if (animator == null)
        {
          UberDebug.LogWarningChannel("mutpersonagens", $"Could not find an animator for character {name}");
        }
      }
      if (charMovement == null)
      {
        charMovement = GetComponent<CharacterMovement>();
      }
    }

    void Update()
    {
      if (animator == null) return;
      animator.SetFloat("planarspeed", charMovement.PlanarSpeed);//, forwardDamp, Time.deltaTime);
      animator.SetFloat("forwardspeed", charMovement.ForwardSpeed, forwardDamp, Time.deltaTime);
      animator.SetFloat("rotationspeedy", charMovement.RotationSpeedY, rotationDamp, Time.deltaTime);
    }

    public void SetEmotion(CharacterEmotion emotion)
    {
      animator.SetInteger("emotion", (int)emotion);
    }
  }
}
