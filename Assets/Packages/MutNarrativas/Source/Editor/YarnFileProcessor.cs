using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using Yarn;

namespace MutNarrativas
{
  public class YarnFileProcessor : AssetPostprocessor
  {
    static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths)
    {
      Dialogue d = new Dialogue(new MemoryVariableStore());
      // TODO: figure if there is a way to click on error and redirect to yarn file
      d.LogDebugMessage = (m) => UberDebug.LogChannel("mutnarrativas", m);
      d.LogErrorMessage = (m) => UberDebug.LogErrorChannel("mutnarrativas", m);
      YarnCustomFunctions.RegisterFunctions(d);
      foreach (string str in importedAssets)
      {
        if (str.Contains(".yarn.txt"))
        {
          UberDebug.LogChannel("mutnarrativas", "Loading Asset: " + str);
          var yarnscript = AssetDatabase.LoadAssetAtPath<TextAsset>(str);
          d.LoadString(yarnscript.text);
          UberDebug.LogChannel("mutnarrativas", "Successfully loaded: " + str);
        }
      }
    }
  }
}