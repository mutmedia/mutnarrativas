mutnarrativas

Projeto do unity pra fazer uns jogos 3d narrativos aí

Documentação
------------

https://gitlab.com/mutmedia/mutnarrativas/wikis/home

Objetivos
---------

Ter um jeito de só arrastar um boneco em t-pose e ele andar por aí (personagem principal)
  - Poder modificar a animação padrao de andar

Ter um jeito de só arrastar um boneco em t-pose e ele ficar na posicao idle (npcs)

Ter um jeito de quando chegar perto de um personagem acontecer uma conversa
  - Ver o Yarn e ver se é muito complexo, se não usar essa parada pra poder ter narrativas profundas

Ter um jeito facil de pegar itens e fazer eles interagirem com o dialogo

Conseguir colocar emocoes no personagem

YARN
====

https://github.com/thesecretlab/YarnSpinner/blob/master/Documentation/YarnSpinner-Dialogue/General-Usage.md

Melhor versão do editor: https://github.com/blurymind/Yarn/releases
