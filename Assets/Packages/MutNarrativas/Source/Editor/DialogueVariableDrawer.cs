
using UnityEditor;
using UnityEngine;

namespace MutNarrativas
{
  [CustomPropertyDrawer(typeof(DialogueVariable))]
  public class DialogueVariableDrawer : PropertyDrawer
  {

    public override float GetPropertyHeight(SerializedProperty prop,
                                                     GUIContent label)
    {
      float totalHeight = 0;

      // Name
      totalHeight += EditorGUIUtility.singleLineHeight;
      // v
      totalHeight += EditorGUIUtility.singleLineHeight;
      // Type
      totalHeight += EditorGUIUtility.singleLineHeight;

      return totalHeight;
    }


    public override void OnGUI(Rect pos, SerializedProperty prop, GUIContent label)
    {
      EditorGUI.BeginProperty(pos, label, prop);

      pos.height = EditorGUIUtility.singleLineHeight;
      //position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

      // Don't make child fields be indented
      var indent = EditorGUI.indentLevel;
      EditorGUI.indentLevel = 0;

      EditorGUI.PropertyField(pos, prop.FindPropertyRelative("Name"));
      pos.y += EditorGUIUtility.singleLineHeight;

      var v = prop.FindPropertyRelative("Variable");
      EditorGUI.PropertyField(pos, v);
      pos.y += EditorGUI.GetPropertyHeight(v);
      //pos.y += EditorGUIUtility.singleLineHeight;

      EditorGUI.PropertyField(pos, prop.FindPropertyRelative("Type"));
      pos.y += EditorGUIUtility.singleLineHeight;

      // Set indent back to what it was
      EditorGUI.indentLevel = indent;

      EditorGUI.EndProperty();
    }
  }
}