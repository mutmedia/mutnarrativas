using System;
using System.Collections.Generic;
using UnityAtoms;
using UnityEngine;
using UnityEngine.Serialization;

namespace MutNarrativas
{
  [Serializable]
  public struct InteractableModel
  {
    [FormerlySerializedAs("CharacterName")]
    public string Name;


    [FormerlySerializedAs("Interactable")]
    public bool CanInteract;

    public bool UseCustomNode;

    public string CustomTalkToNode;

    public string TalkToNode => UseCustomNode ? CustomTalkToNode : Name;

    public TextAsset ScriptToLoad;

    [FormerlySerializedAs("CharacterText")]
    public StringVariable Text;

    public CharacterDialogueBox DialogueBox;

    public GameObject VisualRepresentation;

    public bool IsUnique;
  }
}